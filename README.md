# Telegram bot
## Purpose
Telegram bot connected to server. Also sends memes and GIFs.  
Link to bot on Telegram: [t.me/bioinfbot](https://t.me/bioinfbot)
## Requirements
- [python 3.8.x](https://www.python.org/downloads/) or higher 
- additional packages (see in `requirements.txt`) 
## Installation
- Download these files from GitLab or clone this repository via https (don't forget to install git):
    ```
    git clone https://gitlab.com/mariiabav/telegram-bot.git
    cd telegram-bot
    ```
    If you want to run app from develop branch:
    ```
    git checkout develop
    ```
- Install dependencies for command line application. Use absolute paths if necessary.
   ```
   pip3 install -r requirements.txt
   ```
   If you use PyCharm, you can run this command from `telegram-bot` project Terminal. Also don't forget to set `python 3.8.x` (or higher) as project interpreter.
## Usage
- Run server from the root folder of repository. Use the commands (use full paths if necessary):
   ```
   cd src
   python3 manage.py runserver
   ```
- Open second command prompt.  
   **Mac:** Right-click the command prompt window icon and select Command Prompt.  
   **Windows:** 
   ```
   start cmd
   ```
   A second command prompt window is opened.  
- Start telegram-bot from second cmd. Use the commands (use full paths if necessary):
   ```
   cd telegram-bot
   cd src
   python3 bioinfbot.py 
   ```
   You can also run bot from folder `telegram-bot/src` using any IDE (PyCharm, for example).
   Now [@bioinfbot](https://t.me/bioinfbot) is working!
## Explanation
Bot commands: 
- `/server_all` : sends all records from our server (from [third](https://gitlab.com/mariiabav/r_shiny-django) lab)
- `/meme` : sends random gif-image with funny comment
- `/start` : sends `Очень жаль, вы проиграли`

You can also say `привет` to bot, and it will greet you.
## Example
- Getting all server records:
<img src=https://sun9-36.userapi.com/nvTacLgzEv-3SSiTTACNGhlnl7j41xsnJXTVNQ/aki39_zVJI8.jpg width = "648" height = "633">  

- Getting gifs:
<img src=https://sun9-62.userapi.com/jjUMhjdkN2whMOz3Hhc3YdK-8ZtcQE-po_NfZw/cP4RMD5yDfI.jpg width = "648" height = "633">  