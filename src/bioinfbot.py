import telebot  # type: ignore
import string
import requests
import json
import re
from telebot.types import Message  # type: ignore

TOKEN = '1468511975:AAFXyMzFJcOwKSK-ZMX7GDkozEFL0dhqlRk'
bot = telebot.TeleBot(TOKEN)


@bot.message_handler(commands=['start'])
def start_message(message: Message) -> None:
    bot.send_message(message.chat.id, 'Очень жаль, вы проиграли')


@bot.message_handler(commands=['server_all'])
def server_message(message: Message) -> None:
    r = requests.get("http://127.0.0.1:8000/api/v1/aminoacids/africa/all/")
    mymessage = re.sub("[][]", "", r.content.decode("utf-8")).\
        replace("},", "}, \n\n").replace(',', ', \n').\
        replace(":", " : ")
    bot.send_message(message.chat.id, mymessage)


@bot.message_handler(commands=['meme'])
def meme_message(message: Message) -> None:
    r = requests.get("https://developerslife.ru/random?json=true")
    content = r.content.decode("utf-8")
    d = json.loads(content)
    bot.send_message(message.chat.id, d["description"] + "\n" + d["gifURL"])


@bot.message_handler(content_types=['text'])
def send_text(message) -> None:
    if message.text.lower().rstrip(string.punctuation) == 'привет':
        bot.send_message(message.chat.id, 'Привет')


if __name__ == '__main__':
    print("Bot is running...")
    bot.polling()
