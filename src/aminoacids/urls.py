from aminoacids.views import AfricaCreateView, AfricaListView, AfricaDetailView
from django.urls import path  # type: ignore

app_name = 'aminoacids'
urlpatterns = [
    path('africa/create/', AfricaCreateView.as_view()),
    path('africa/all/', AfricaListView.as_view()),
    path('africa/detail/<int:pk>/', AfricaDetailView.as_view())
]
